"""django_test URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from rest_framework_swagger.views import get_swagger_view
from rest_auth.views import UserDetailsView
from .views import main_page



urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^accounts/', include('allauth.urls')),

    # in this api line returns registered user
    url(r'^user/$', UserDetailsView.as_view(), name='rest_user_details'),
    # api for auth
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    # api and gui for swager
    url(r'^api/docs/', get_swagger_view(title='api_docs')),
    # my main api
    url(r'^api/', include('poll.urls')),
    # ui for app
    url(r'^$', main_page),
]
