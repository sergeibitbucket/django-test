from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404, redirect, render

# main gui
@login_required()
def main_page(request):
    return render(request,'page.html')
