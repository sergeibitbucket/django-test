var poll_ui = angular.module('poll_ui', ['ngRoute','ngResource', 'chart.js']);

poll_ui.config(function ( $httpProvider) {

    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
});

poll_ui.config(function($resourceProvider) {
  $resourceProvider.defaults.stripTrailingSlashes = false;
});


poll_ui.factory('QuestionResource', function ($resource) {
    return $resource('/api/question/:id',{ id: '@_id' },{
        update: {method: 'PATCH'},
        get_random: {method: 'GET', url:'/api/question/get_random/', isArray:false},
        set_answer: {method:'POST', url:'/api/question/:id/set_answer/', params: {id: '@_id'}}
  });
});

poll_ui.factory('AnswerResource', function ($resource) {
    return $resource('/api/answer/:id',{ id: '@_id' },{
        update: {method: 'PATCH'},
        for_question: {method: 'GET', url:'/api/answer/:id/for_question/', params: {id: '@_id'}, isArray: true},
  });
});

poll_ui.factory('AuthResource', function ($resource) {
    return $resource('/api/user/:id/',{ id: '@_id' },{
        get_user: {method: 'GET', url:'/user/'}
  });
});