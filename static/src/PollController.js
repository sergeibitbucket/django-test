poll_ui.controller('PollController',function( $scope, AuthResource, QuestionResource, AnswerResource) {

    var vm = this;
    vm.answered = false;

    vm.chart_labels = [];
    vm.chart_data = [];
    vm.chart_colors = ["rgba(75,192,192,0.4)"];
    vm.chart_options = {legend: {display: true}};


    vm.user_details = AuthResource.get_user(function(){
    });

    vm.question = QuestionResource.get_random(function(data){
        AnswerResource.for_question({id:data.id}, function(data1){
            vm.list_answers = data1;
        });
    });

    vm.send_answer = function(item){
        QuestionResource.set_answer({id:vm.question.id}, item, function(data){
        });
        vm.list_answers.forEach(function(item ,i ,err){
            vm.chart_labels.push(item.answer);
        });
        vm.list_answers.forEach(function(item ,i ,err){
            vm.chart_data.push(item.count);
        });
        vm.answered = true;
    }

});
