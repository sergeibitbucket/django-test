import random

from rest_framework.permissions import IsAuthenticated
from rest_framework import viewsets
from rest_framework.decorators import list_route, detail_route
from rest_framework.response import Response
from rest_framework import status

from .serializers import QuestionSerializer, AnswerSerializer
from .models import Question, Answer
from .permissions import Permission


# there is main file of logic for django-rest


# logic for question
class QuestionApiView(viewsets.ViewSet):
    permission_classes = (IsAuthenticated,)

    def list(self, request): # CRUD + list
        perm = Permission(request) # can user get list?

        if perm.read_permission(): # if can then
            recs = Question.objects.all() # get list
            ser = QuestionSerializer(recs, many=True) # load it into json parser
            return Response(ser.data) # return to browser

        else:
            return Response('Permissions Failed', status=status.HTTP_401_UNAUTHORIZED)

    # CRUD + list
    def create(self, request):
        perm = Permission(request) # same
        if perm.create_permission():
            ser = QuestionSerializer(data=request.data) # load json data from browser request
            if ser.is_valid(): # check it
                ser.save() # save it
                return Response(ser.data) # return code 200 and this data
            else:
                return Response(ser.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response('Permissions Failed', status=status.HTTP_401_UNAUTHORIZED)

    # CRUD + list
    def update(self, request, pk):
        perm = Permission(request)
        if perm.update_permission():
            ser = QuestionSerializer(Question.objects.get(id=pk), data=request.data, partial=True) # same for update
            if ser.is_valid():
                ser.save()
                return Response(ser.data)
            else:
                return Response(ser.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response('Permissions Failed', status=status.HTTP_401_UNAUTHORIZED)

    # CRUD + list
    @staticmethod
    def destroy(request, pk):
        perm = Permission(request)
        rec = Question.objects.get(id=pk)
        if perm.delete_permission(): # same for deleta
            rec.delete()
            return Response('Deleted', status=status.HTTP_204_NO_CONTENT)
        else:
            return Response('Permissions Failed', status=status.HTTP_401_UNAUTHORIZED)

    @list_route()
    def get_random(self, request): # this is not standart CRUD functions. they are parsed 2 situations (in question) - get random question and answer on a question
        perm = Permission(request)
        if perm.read_permission(): # can user get question

            id = random.randint(0, Question.objects.count() - 1) # get rand id
            question = Question.objects.all()[id] # get question with it id
            ser = QuestionSerializer(question) # same as cRud
            return Response(ser.data)
        else:
            return Response('Permissions Failed', status=status.HTTP_401_UNAUTHORIZED)

    @detail_route(methods=['POST'])
    def set_answer(self, request, pk):
        perm = Permission(request)
        if perm.read_permission():
            question = Question.objects.get(id=pk) # save answer for a question. get question by id
            answer = question.answers.get(id=request.data['id']) # get answer by id
            answer.count = answer.count + 1 # increase count of answers
            answer.save() # save data
            return Response('Accepted', status=status.HTTP_202_ACCEPTED)
        else:
            return Response('Permissions Failed', status=status.HTTP_401_UNAUTHORIZED)

# logic for answer. almoust the same. except 1 function
class AnswerApiView(viewsets.ViewSet):
    permission_classes = (IsAuthenticated,)

    def list(self, request):
        perm = Permission(request)

        if perm.read_permission():
            recs = Answer.objects.all()
            ser = AnswerSerializer(recs, many=True)
            return Response(ser.data)
        else:
            return Response('Permissions Failed', status=status.HTTP_401_UNAUTHORIZED)

    def create(self, request):
        perm = Permission(request)
        if perm.create_permission():
            ser = AnswerSerializer(data=request.data)
            if ser.is_valid():
                ser.save()
                return Response(ser.data)
            else:
                return Response(ser.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response('Permissions Failed', status=status.HTTP_401_UNAUTHORIZED)

    def update(self, request, pk):
        perm = Permission(request)
        if perm.update_permission():
            ser = AnswerSerializer(Answer.objects.get(id=pk), data=request.data, partial=True)
            if ser.is_valid():
                ser.save()
                return Response(ser.data)
            else:
                return Response(ser.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response('Permissions Failed', status=status.HTTP_401_UNAUTHORIZED)

    @staticmethod
    def destroy(request, pk):
        perm = Permission(request)
        rec = Answer.objects.get(id=pk)
        if perm.delete_permission():
            rec.delete()
            return Response('Deleted', status=status.HTTP_204_NO_CONTENT)
        else:
            return Response('Permissions Failed', status=status.HTTP_401_UNAUTHORIZED)


    @detail_route(methods=['GET'])
    def for_question(self, request, pk=None): # in this function we return answers for one question
        perm = Permission(request)
        question = Question.objects.get(id=pk) # get this question by id
        if perm.read_permission():
            recs = question.answers.all() # get answers of this question
            ser = AnswerSerializer(recs, many=True)
            return Response(ser.data) # return it
        else:
            return Response('Permissions Failed', status=status.HTTP_401_UNAUTHORIZED)    \

