from rest_framework import serializers
from django.contrib.auth import get_user_model
from .models import Answer, Question


UserModel = get_user_model()


# its a json-data-json parsers used by django-rest framework (standart of django-rest model)
class QuestionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Question
        fields = "__all__"

class AnswerSerializer(serializers.ModelSerializer):

    class Meta:
        model = Answer
        fields = "__all__"