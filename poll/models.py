from django.db import models

# Create your models here.

# answer model. pretty simple
class Answer(models.Model):
    answer = models.CharField(max_length=256)
    is_true = models.BooleanField()
    count = models.IntegerField(default=0)

# question model. it have only text field and links to answers
class Question(models.Model):
    question = models.TextField()

    answers = models.ManyToManyField(Answer)