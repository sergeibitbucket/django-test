from .views import QuestionApiView, AnswerApiView
from rest_framework import routers

router = routers.DefaultRouter()
# here is the 2 endpoint of my api
router.register(r'question', QuestionApiView, base_name='api_question')
router.register(r'answer', AnswerApiView, base_name='api_answer')

urlpatterns = [] + router.urls