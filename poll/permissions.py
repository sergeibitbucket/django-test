from abc import ABCMeta, abstractmethod


# permission model for application. only registered users can read and vote only superuser can add questions and answers
class Permission():

    def __init__(self, request):
        self.request = request

    #Crud
    def create_permission(self):
        if self.request.user.is_superuser:
            return True
        else:
            return False

    #crUd
    def update_permission(self):
        if self.create_permission():
            return True
        else:
            return False

    #cRud
    def read_permission(self):
        return True

    #cruD
    def delete_permission(self):
        if self.create_permission():
            return True
        else:
            return False

    # for answer
    def set_answer_permission(self):
        if self.request.user:
            return True
        else:
            return False